class Game:
    rolls = []
    current_roll = 0

    def __init__(self):
        self.total_score = 0
    
    def roll(self, pins):
        self.rolls.append(pins)
        self.current_roll += 1
    
    def is_spare(self, index):
        return (self.rolls[index] + self.rolls[index+1]) == 10

    def is_strike(self, index):
        return (self.rolls[index]==10)
    
    def sum_of_balls_in_frame(self, index):
        return self.rolls[index] + self.rolls[index+1]

    def spare_bonus(self, index):
        return self.rolls[index+2]
    
    def strike_bonus(self, index):
        return self.rolls[index+1] + self.rolls[index+2]

    def score(self):
        total_score = 0
        index = 0
        for frame in range(10):
            if (self.is_strike(index)):
                total_score += 10 + self.strike_bonus(index)
                index +=1
            elif (self.is_spare(index)):
                total_score += 10 + self.spare_bonus(index)
                index +=2
            else: 
                total_score += self.sum_of_balls_in_frame(index)
                index += 2
        return total_score
