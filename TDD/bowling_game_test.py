import pytest
from bowling_game import Game

g = Game()


@pytest.fixture
def setup():
    # Nullify the total score for all the test functions on start
    g.rolls = []
    g.current_roll = 0
    g.total_score = 0 

def roll_many(n, pins):
    for i in range(n):
        g.roll(pins)

def roll_spare():
    g.roll(5)
    g.roll(5)

def roll_strike():
    g.roll(10)

def test_first_game(setup):
    roll_many(20, 0)
    assert g.score() == 0


def test_second_all_ones(setup):
    roll_many(20, 1)
    assert g.score() == 20

def test_one_spare(setup):
    roll_spare()
    g.roll(3)
    roll_many(17,0)
    assert g.score() == 16

def test_strike(setup):
    roll_strike()
    g.roll(3)
    g.roll(4)
    roll_many(16,0)
    assert g.score() == 24

def test_perfect_game(setup):
    roll_many(12,10)
    assert g.score() == 300